export const currentSpeed = {
	currentSpeedText: {
		type: 'text',
		svg: {
			init: () => `<text></text>`,
			update: function(node, args) {
				node.textContent = parseFloat(args.speed).toFixed(2) + ' m/s'
			}
		}
	},
	currentSpeedBarChart: {
		type: 'sliderBarChart',
		schema: {
			w: {type:'number', default:.05},
			h: {type:'number', default:.1},
			speed: {type:'fill', default:'#FFFFFF'},
			background: {type:'fill', default:'#000000'},
		},
		svg: {
			init: () => `<g class="sliderBarChart">
				<rect></rect>
				<rect></rect>
			</g>`,
			update: function(node, args) {
				const {w, h} = this
				const percent = args.speed / 2.2 // TODO compute speed max from data

				node.setAttribute('transform', `translate(${-w/2},${-h/2})`)

				node.children[0].style.fill = this.background
				node.children[1].style.fill = this.speed

				node.children[0].setAttribute('x', -this.w * .5)
				node.children[1].setAttribute('x', -this.w * .5)

				node.children[0].setAttribute('width', this.w)
				node.children[1].setAttribute('width', percent * this.w)

				node.children[0].setAttribute('height', this.h)
				node.children[1].setAttribute('height', this.h)
			}
		}
	},
	currentSpeedGlyph: {
		type: 'glyph',
		schema: {
			w: {type:'number', default:.05},
			h: {type:'number', default:1},
			fill: {type:'fill', default:'#FFFFFF'},
			stroke: {type:'stroke', default:'#FFFFFF00'},
		},
		svg: {
			init: () => `<g class="glyph">
				<path></path>
			</g>`,
			update: function(node, args) {
				const {w, h} = this
				const percent = Math.min(args.speed / 2.2, 1) // TODO compute speed max from data

				node.style.transform = `rotate(${this.flipFactor === 1 ? 0 : 180}deg)`
				node.children[0].style.transform = `translate(${-w/2}px,${-h/2}px)`

				node.children[0].setAttribute('d', `
					M ${w/2} ${percent*h}
					A 42 42 0 0 0 ${w/2} ${(1-percent)*h}
					L 0 ${h/2} Z
				`)
			}
		}
	}
}