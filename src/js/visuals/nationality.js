export const nationality = {
	nationalFlags: {
		type: 'flag',
		colors: {
			FR: ['#002654', '#FFFFFF', '#ED2939'],
			BE: ['#2D2926', '#FFCD00', '#C8102E']
		},
		schema: { // https://aframe.io/docs/1.4.0/core/component.html#property-types
			w: {type:'number', default:.025},
			h: {type:'number', default:.5},
		},
		svg: {
			init: function(args) {
				const {w, h} = this
				const colors = this.colors[args.nationality]
				return `
			<g class="flag" transform="translate(${-w/2},${-h/2})">
				<rect x="${-w}" y="0" width="${w}" height="${h}" fill="${colors[0]}"></rect>
				<rect x="0" y="0" width="${w}" height="${h}" fill="${colors[1]}"></rect>
				<rect x="${w}" y="0" width="${w}" height="${h}" fill="${colors[2]}"></rect>
			</g>`
			},
			update: function(node) {
				const {w, h} = this
				node.setAttribute('transform', `translate(${-w/2},${-h/2})`)
				node.children[0].setAttribute('x', -w)
				node.children[2].setAttribute('x', w)
				for (const child of node.children) {
					child.setAttribute('width', w)
					child.setAttribute('height', h)
				}
			}
		}
	},
	nationalText: {
		type: 'text',
		schema: {
			fill: {type:'color', default:'#FFFFFF'},
			stroke: {type:'color', default:'#FFFFFF00'}
		},
		svg: {
			init: function(args) {
				return `<text>${args.nationality}</text>`
			}
		}
	}
}