import {visuals} from './visuals.js'

// const CANVAS = 'canvas'
// const SVG = 'svg'
// const MODE = SVG
const SVG_WIDTH = 1920
const SVG_HEIGHT = 1080

// FORMS
const controllers = document.forms.controllers.elements
const flayers = document.forms.layers.elements

const CSV = {
	// from text to object with headers as key and value parsed as numbers if possible
	parse: function(text) {
		const data = []
		const lines = text.split('\n')
		const head = lines[0].split(',')
		for (let i = 1; i < lines.length; i++) {
			if (lines[i] === '') continue
			const lineSplit = lines[i].split(',')
			const lineData = {}
			for (let j = 0; j < head.length; j++) {
				let val = lineSplit[j]

				const valNumber = Number.parseFloat(val)
				if (!Number.isNaN(valNumber)) {
					val = valNumber
				}

				lineData[head[j]] = val
			}
			data[i-1] = lineData
		}
		return data;
	}
}

// from csv to data[frameId][swimmerId]
// and some key transformed
function dataFromCsv(csv) {
	const transform_key = {
		xa_above: 'x_left',
		xb_above: 'x_right',
	}
	const data = {}
	for (let frame of csv) {
		if (!data[frame.frameId]) {
			data[frame.frameId] = {}
		}
		for (let key in frame) {
			if (transform_key[key]) {
				frame[transform_key[key]] = frame[key]
			}
		}
		data[frame.frameId][frame.swimmerId] = frame
	}
	return data
}

async function loadData() {
	const res = await fetch('data.csv')
	const text = await res.text()
	return dataFromCsv(CSV.parse(text))
}


function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

const el = {
	properties: document.querySelector('#properties'),
	video: document.querySelector('video'),
	canvas: document.querySelector('canvas'),
	svg: document.querySelector('svg'),
	// currentLayer: document.querySelector('#currentLayer'),
	timestamp: document.querySelector('#timestamp'),
	progressBar: document.querySelector('#progressBar'),
	layerList: document.querySelector('#layers ul'),
}

function createCheckbox(name, label) {
	return `
		<div class="input">
			<input type="checkbox" name="${name}" checked>
			<label for="${name}">${label}</label>
		</div>
	`
}
function createTextInput(name, labelContent, value) {
	return `
		<div class="input">
			<label for="${name}">${labelContent}</label>
			<input type="text" name="${name}" value="${value}">
		</div>
	`
}

function html(text) {
	const e = document.createElement('div')
	e.innerHTML = text
	return e.firstElementChild
}

function svg(text) {
	const e = document.createElementNS("http://www.w3.org/2000/svg", "g");
	e.innerHTML = text
	return e.firstElementChild
}

function _for(n, f) {
	let ret = ''
	for (let i = 0; i < n; i++) {
		ret += f(i)
	}
	return ret
}

function property(text) {
	const ret = html(text)

	ret.append = () => {
		el.properties.appendChild(ret)
	}

	return ret
}

// return values of inputs per fieldset in properties
// TODO only write values !== default value
function getPropertyValues() {
	const entries = Object.fromEntries(
		new FormData(document.forms.controllers)
	)
	const data = {
		category: entries.category
	}
	for (const el of controllers) {
		if (el.tagName === 'FIELDSET') {
			data[el.name] = {}
			for (const subel of el.elements) {
				if (subel.name in entries) {
					if (subel.type === 'radio') {
						if (subel.checked) {
							data[el.name][subel.name] = subel.value
						}
					} else {
						data[el.name][subel.name] = subel.value
					}
				}
			}
		}
	}
	return data
}

const properties = {
	displayFilter: () => property(`
		<fieldset name="display">
			<label name="filter">Display Filter</label>
			<select name="filter">
				<option value="lanes">Lanes</option>
			</select>
			<fieldset name="lanes">
				${_for(8, i => createCheckbox('lane-'+i, i+1))}
			</fieldset>
			<style>
				fieldset[name="lanes"] {
					display: flex;
					flex-wrap: wrap;
					justify-content: space-between;
					gap: 4px;
				}

				fieldset[name="lanes"] * {
					flex-basis: 21%;
				}
			</style>
		</fieldset>
	`),
	transform: schema => {
		const prop = property(`
		<fieldset name="transform">
			${createCheckbox('move', 'Move with Swimmer')}
			${createCheckbox('flip', 'Flip')}
			${createTextInput('x', 'X', 0)}
			${createTextInput('y', 'Y', 0)}
			${createTextInput('r', 'R', 0)}
			${schema && schema.w ? createTextInput('w', 'W', schema.w.default) : ''}
			${schema && schema.h ? createTextInput('h', 'H', schema.h.default) : ''}
			${schema && schema.w && schema.h ? createCheckbox('lockRatio', '🔒') : ''}
			<div class="input">
				<label name="opacity">Opacity</label>
				<input type="text" name="opacity" value="100%">
			</div>
			<style>
				fieldset[name="transform"] {
					display: grid;
					grid-template-columns: repeat(3, 1fr);
					column-gap: 4px;
				}

				fieldset[name="transform"] .input:nth-child(1),
				fieldset[name="transform"] .input:nth-child(2),
				fieldset[name="transform"] .input:nth-last-child(2){
					grid-column: 1 / 4;
					justify-content: space-between;
				}
			</style>
		</fieldset>
	`)
		if (schema && schema.w && schema.h) {
			const lockRatioInput = prop.querySelector('input[name="lockRatio"]')
			const wInput = prop.querySelector('input[name="w"]')
			const hInput = prop.querySelector('input[name="h"]')
			wInput.addEventListener('change', evt => {
				if (lockRatioInput.checked) {
					hInput.value = evt.target.value * (hInput.defaultValue / evt.target.defaultValue)
				}
			})
			hInput.addEventListener('change', evt => {
				if (lockRatioInput.checked) {
					wInput.value = evt.target.value * (wInput.defaultValue / evt.target.defaultValue)
				}
			})
			lockRatioInput.addEventListener('change', evt => {
				if (evt.target.checked) {
					wInput.defaultValue = wInput.value
					hInput.defaultValue = hInput.value
				}
			})
		}
		return prop
	},
	text: () => property(`
		<fieldset name="text">
			<h1>Text</h1>
			<select name="font">
				<option value="Arial">Arial</option>
				<option value="Georgia">Georgia</option>
			</select>
			<select name="fontStyle">
				<option value="Regular">Regular</option>
			</select>
			<input type="text" name="fontSize" value="5em">
			<style>
				fieldset[name="text"] {
					display: grid;
					grid-template-columns: repeat(2, 50%);
					column-gap: 4px;
				}

				fieldset[name="text"] h1 {
					grid-column: 1 / 3;
				}

				fieldset[name="text"] select[name="font"] {
					grid-column: 1 / 3;
				}
			</style>
		</fieldset>
	`),
	color: ({name, value = '#FFFFFF', transparency = '100%', stroke = 1}) => {
		const prop = property(`
			<fieldset name="${name}" class="color">
				<h1>${capitalizeFirstLetter(name)}</h1>
				<button type="button" name="color" class="colorBtn"></button>
				<input type="text" name="hex" value="${value.slice(1)}">
				<input type="text" name="transparency" value="${transparency}">
				${stroke !== null ? `<input type="text" name="width" value="${stroke}">` : ''}
				<style>
						fieldset[name="${name}"] {
							display: grid;
							grid-template-columns: 15% 35% 25% 25%;
							align-items: center;
						}
						fieldset[name="${name}"] h1 {
							grid-column: 1 / 5;
						}
						fieldset[name="${name}"] .colorBtn {
							all: unset;
							width: 1em;
							height: 1em;
							--btn-background-color: ${value};
							background-color: var(--btn-background-color);
							border: var(--border-color) solid 1px;
						}
				</style>
			</fieldset>
		`)
		prop.addEventListener('input', evt => {
			const els = evt.currentTarget.elements
			evt.currentTarget.elements.color.style.setProperty(
				'--btn-background-color',
				'#' + els.hex.value //+ percentInHex(els.transparency.value),
			)
		})
		prop.elements.color.addEventListener('click', () => console.log('TODO open color picker'))
		return prop
	},
	style: () => property(`
		<fieldset name="style">
			<h1>Style</h1>
			<textarea name="generatedCss" cols="20" rows="10" disabled style="display: none"></textarea>
			<textarea name="css" cols="20" rows="15"></textarea>
			<style>
				fieldset[name="style"] textarea {
					background-color: var(--background-color);
					color: var(--text-color);
				}
			</style>
		</fieldset>
	`),

	/*
	time: function() {},
	fade: function() {},
	 */
}

function removeChildren(node) {
	while (node.firstChild) {
		node.removeChild(node.lastChild);
	}
}

const ignoredProperties = ['w', 'h']
function updateProperties(module) {
	console.log('update properties', module)

	removeChildren(el.properties)

	let props = [
		properties.displayFilter(),
		properties.transform(module.schema)
	]

	switch (module.type) {
		case 'text':
			props = [
				...props,
				properties.text(),
				properties.color({name:'fill', stroke: null}),
				properties.color({name:'stroke', transparency:'0%'})
			]
			break
		default:
			for (const propset in module.schema) {
				if (ignoredProperties.includes(propset)) continue
				if (module.schema[propset].type) {
					const prop = module.schema[propset]
					if (prop.type === 'fill') {
						props.push(properties.color({name:propset, stroke: null, value:prop.default}))
					} else if (prop.type === 'stroke') {
						props.push(properties.color({name:propset, transparency:'0%', value:prop.default}))
					} else {
						throw new Error(`undefined prop type ${prop.type}`)
					}
				}/* else {
					for (const propName in module.schema[propset]) {
						const prop = module.schema[propset][propName]
						debugger
					}
				}*/
			}
	}

	props.push(properties.style())

	for (let prop of props) {
		prop.append()
	}
}

function onVisTypeChanged(evt) {
	const visual = visuals.get(evt.target.value)
	console.log('onVisTypeChanged', evt.target.value, visual)

	// TODO TEST
	const layer = document.querySelector('li.layer.current')
	if (layer && layer.__layer__) layer.__layer__.initialized = []

	updateProperties(visual)
}

controllers.category.addEventListener('change', evt => {
	removeChildren(controllers.selectType)
	removeChildren(el.properties)

	const val = evt.target.value
	const category = visuals.category(val)
	for (const name in category) {
		const module = category[name]

		const button = document.createElement('button')
		button.setAttribute('type', 'button')
		controllers.selectType.append(button)

		const inputVisType = document.createElement('input')
		inputVisType.setAttribute('name', 'visType')
		inputVisType.setAttribute('value', `${val}.${name}`)
		inputVisType.setAttribute('type', 'radio')
		inputVisType.addEventListener('change', onVisTypeChanged)
		button.append(inputVisType)

		const image = document.createElement('img')
		if (module.type === 'text') {
			image.setAttribute('src', 'img/png/icon/text.png')
		} else if (module.type === 'flag') {
			image.setAttribute('src', 'img/png/icon/flags.png')
		} else if (module.type === 'sliderBarChart') {
			image.setAttribute('src', 'img/png/icon/sliderBarChart.png')
		} else if (module.type === 'glyph') {
			image.setAttribute('src', 'img/png/icon/glyph.png')
		}
		button.append(image)
		button.addEventListener('click', () => {
			inputVisType.checked = true
			inputVisType.dispatchEvent(new Event('change', {bubbles: true})) // TODO WHY NOTHING FIRE
		})
	}
})

function loadVisuals() {
	fetch('js/visuals/list.json')
	.then(res => res.json())
	.then(visuals.load)
	.then(() => {
		const categories = visuals.categories()
		for (const category of categories) {
			const option = document.createElement('option')
			option.setAttribute('value', category)
			option.textContent = capitalizeFirstLetter(category) + ' (98%)'
			controllers.category.appendChild(option)
		}
		controllers.category.removeAttribute('disabled')
	})
}

async function loadVideo({autoplay} = {}) {
	return new Promise((resolve, reject) => {
		const localVideoUrl = '2021_Montpellier_100_brasse_birdseyes.mp4'
		const remoteVideoUrl = 'https://files.inria.fr/aviz/vis-in-motion-swimflow/2021_Montpellier_100_brasse_birdseyes.mp4'
		const isLocal = () => location.hostname === 'localhost' || location.hostname === '127.0.0.1'
		el.video.addEventListener('error', evt => {
			if (evt.target.getAttribute('src') !== remoteVideoUrl) {
				el.video.setAttribute('src', remoteVideoUrl)
			} else {
				reject()
			}
		})
		el.video.addEventListener('loadeddata', () => {
			if (autoplay) el.video.play()
			resolve()
		})
		el.video.addEventListener('play', () => {
			document.querySelector('#playPauseBtn').classList.remove('play')
			document.querySelector('#playPauseBtn').classList.add('pause')
		})
		el.video.addEventListener('pause', () => {
			document.querySelector('#playPauseBtn').classList.add('play')
			document.querySelector('#playPauseBtn').classList.remove('pause')
		})
		el.video.setAttribute('src', isLocal() ? localVideoUrl : remoteVideoUrl)
		el.video.setAttribute('loop', 'infinite')
	})
}

document.querySelector('#playPauseBtn').addEventListener('click', evt => {
	if (evt.target.classList.contains('pause')) {
		el.video.pause()
	} else {
		el.video.play()
	}
})

function clearCanvas() {
	el.canvas.getContext('2d').clearRect(0, 0, el.canvas.offsetWidth, el.canvas.offsetHeight)
}

function updateSize(WIDTH, HEIGHT) {
	el.canvas.setAttribute('width', WIDTH)
	el.canvas.setAttribute('height', HEIGHT)

	el.svg.setAttribute('width', WIDTH)
	el.svg.setAttribute('height', HEIGHT)
}

function percentValue(percent) {
	return Number.parseFloat(Number.parseInt(percent.slice(0, -1))) / 100
}

// string input in % like '25%', give the hex value
function percentInHex(percent) {
	return ((percentValue(percent) * 255) | 1 << 8).toString(16).slice(1).toUpperCase()
}

// const totalDistance = 100
const halfDistance = 50
function drawCurrentVis(frame, metadata) {
	if (!frame) return

	const currentVisInput = controllers.visType?.value//document.querySelector('input[name="visType"]:checked')
	if (!currentVisInput) return

	const currentVis = visuals.get(currentVisInput)()

	const ctx = el.canvas.getContext('2d')

	const WIDTH = el.canvas.offsetWidth
	const HEIGHT = el.canvas.offsetHeight
	const laneWidth = WIDTH
	const laneHeight = HEIGHT / metadata.lanes

	for (let lane = 0; lane < metadata.lanes; lane++) {
		if (!frame[lane]) continue

		// display filter by lanes
		if (controllers.display.elements.filter.value === 'lanes' &&
			!controllers.display.elements.lanes.elements[lane].checked) continue

		ctx.save()

		ctx.beginPath()
		ctx.rect(0, laneHeight * lane, laneWidth, laneHeight)
		ctx.clip()

		const x = parseFloat(controllers.transform.elements.x.value)
		const y = parseFloat(controllers.transform.elements.y.value)
		const moveWithSwimmer = controllers.transform.elements.move.checked
		const shouldFlip = controllers.transform.elements.flip.checked
		const flipFactor = !shouldFlip ? 1 : frame[lane].direction === 'advance' ? 1 : -1
		ctx.translate(
			(moveWithSwimmer ? 1 : 0) * (frame[lane].x_middle / halfDistance * laneWidth) +
				x * flipFactor * laneWidth/100,
			laneHeight / 2 + (laneHeight * lane) +
				y * laneHeight/100
		)
		ctx.rotate(controllers.transform.elements.r.value * Math.PI / 180)

		ctx.globalAlpha = percentValue(controllers.transform.elements.opacity.value)

		try {
			currentVis.draw({
				...currentVis,
				ctx: ctx,

				x: 0,//controllers.transform.elements.x.value,
				y: 0,//controllers.transform.elements.y.value,

				w: currentVis.w * laneWidth,
				h: currentVis.h * laneHeight,

				font: controllers.text?.elements.font.value,
				fontSize: controllers.text?.elements.fontSize.value,

				colors: {
					...currentVis.colors,
					fill: (controllers.fill && '#' + controllers.fill.elements.hex.value + percentInHex(controllers.fill.elements.transparency.value)),
					stroke: (controllers.stroke &&'#' + controllers.stroke.elements.hex.value + percentInHex(controllers.stroke.elements.transparency.value))
				},

				strokeWeight: controllers.stroke?.elements.width.value,

				args: frame[lane],
				// edge: edge,
				// vis: vis,
				// i: i
			})
		} catch(e) {
			console.error(e)
		}
		ctx.restore()


		// SVG
		// const svgLane = el.currentLayer.children[lane]
		// const svgLaneWidth = SVG_WIDTH
		// const svgLaneHeight = SVG_HEIGHT/metadata.lanes
		// const svgX = (moveWithSwimmer ? 1 : 0) * (frame[lane].x_middle / halfDistance * svgLaneWidth) +
		// 	x * flipFactor * svgLaneWidth/100 + (!moveWithSwimmer && flipFactor === -1 ? svgLaneWidth : 0)
		// const svgY = svgLaneHeight / 2 + y * svgLaneHeight/100
		//
		// svgLane.removeAttribute('style')
		// svgLane.style.clipPath = `url(#cp-lane)`
		// svgLane.style.opacity = percentValue(controllers.transform.elements.opacity.value)
		// svgLane.style.transform = `translate(0px, ${lane * svgLaneHeight}px)`
		// svgLane.firstElementChild.style.transform = `translate(${svgX}px, ${svgY}px) rotate(${controllers.transform.elements.r.value}deg)`
		// svgLane.style.fontFamily = controllers.text?.elements.font.value
		// svgLane.style.fontSize = controllers.text?.elements.fontSize.value
		// svgLane.style.fill = (controllers.fill && '#' + controllers.fill.elements.hex.value + percentInHex(controllers.fill.elements.transparency.value))
		// svgLane.style.stroke = (controllers.stroke &&'#' + controllers.stroke.elements.hex.value + percentInHex(controllers.stroke.elements.transparency.value))
		// svgLane.style.strokeWidth = controllers.stroke?.elements.width.value
		// svgLane.style.textAnchor = 'middle'
		// svgLane.style.dominantBaseline = 'middle'
		// controllers.style.elements.generatedCss.value = svgLane.style.cssText.replaceAll('; ', ';\n')
		// svgLane.setAttribute(
		// 	'style',
		// 	svgLane.style.cssText + '\n' + controllers.style.elements.css.value
		// )
		//
		// const visThis = {...currentVis}
		// if (controllers.transform.elements.w) {
		// 	visThis.w = controllers.transform.elements.w.value * svgLaneWidth
		// }
		// if (controllers.transform.elements.h) {
		// 	visThis.h = controllers.transform.elements.h.value * svgLaneHeight
		// }
		//
		// if (!currentVis.svg.initialized[lane] && currentVis.svg.init) {
		// 	svgLane.firstElementChild.innerHTML = currentVis.svg.init.call(visThis, frame[lane])
		// 	currentVis.svg.initialized[lane] = true
		// }
		// if (currentVis.svg.update) {
		// 	currentVis.svg.update.call(visThis, svgLane.firstElementChild.firstElementChild, frame[lane])
		// }
	}
}

const framerate = 50
const duration_per_frame_seconds = (1/framerate).toFixed(2)
const metadata = {
	lanes: 8,
	raceStartTime: 13.7
}

async function init() {
	loadVisuals()

	await loadVideo({autoplay: true})

	const raceStartPercent = metadata.raceStartTime/el.video.duration*100
	document.forms.controls.duration.value = el.video.duration.toFixed(2)

	const data = await loadData()
	console.log('loaded data', data)

	// SVG INIT LANES
	el.svg.appendChild(svg(`
		<defs>
			<clipPath id="cp-lane">
				<rect x="0" y="0" width="${SVG_WIDTH}" height="${SVG_HEIGHT/metadata.lanes}" />
			</clipPath>
		</defs>
	`))

	updateSize(el.video.offsetWidth, el.video.offsetHeight)

	function update() {

		if (!el.video.paused) {
			document.forms.controls.current.value = el.video.currentTime.toFixed(2)
		}

		const progressBar = document.forms.controls.progressBar
		const progressBarPos = el.video.currentTime / el.video.duration * 100
		progressBar.value = progressBarPos

		if (el.video.currentTime < metadata.raceStartTime) {
			progressBar.style.backgroundImage = `linear-gradient(to right, #878787 0%, #878787 ${progressBarPos}%, #d3d3d3 ${progressBarPos}%, #d3d3d3 ${raceStartPercent}%, #BEE6F4 ${raceStartPercent}%)`
		} else {
			progressBar.style.backgroundImage = `linear-gradient(to right, #878787 0%, #878787 ${raceStartPercent}%, #3C6CA8 ${raceStartPercent}%, #3C6CA8 ${progressBarPos}%, #BEE6F4 ${progressBarPos}%)`
		}

		// let the video play in a loop from the position with data
		let currentTime = el.video.currentTime
		if (currentTime < metadata.raceStartTime) {
			el.video.currentTime = metadata.raceStartTime
		}

		const currentFrameId = Math.floor(el.video.currentTime / duration_per_frame_seconds)
		const currentFrame = data[currentFrameId]

		clearCanvas()
		if (currentFrame) {
			// drawCurrentVis(currentFrame, metadata)
			layers.update(currentFrame)
		}

		window.requestAnimationFrame(update)
	}
	window.requestAnimationFrame(update)


	// function test() {
	// 	flayers.addBtn.dispatchEvent(new Event('click'))
	//
	// 	controllers.category.value = 'nationality'
	// 	controllers.category.dispatchEvent(new Event('change', { bubbles:true }))
	//
	// 	const typeBtn = document.querySelector('input[value="nationalFlags"]')
	// 	typeBtn.checked = true
	// 	typeBtn.dispatchEvent(new Event('change', { bubbles:true }))
	//
	// 	controllers.transform.elements.w.value = 0.03
	// 	controllers.transform.elements.w.dispatchEvent(new Event('change', { bubbles:true }))
	// }
	// test()
}
init()

window.addEventListener('resize', () => {
	updateSize(0,0)
	updateSize(el.video.offsetWidth, el.video.offsetHeight)
}, true);

const layers = {
	create: () => {
		const layer = html(`
			<li class="layer">
				<button type="button" name="visibilityBtn"><img src="img/svg/eye.svg" /></button>
				<input type="text" name="name" value="Layer ${el.layerList.children.length+1}" />
				<button type="button" name="duplicateBtn"><img src="img/svg/duplicate.svg" /></button>
				<button type="button" name="deleteBtn"><img src="img/svg/trash.svg" /></button>
			</li>
		`)
		layer.addEventListener('click', evt => {
			layers.select(evt.currentTarget)
		})
		layer.querySelector('input[name="name"]').addEventListener('input', evt => {
			if (layers.current() === layer) {
				document.forms.controllers.querySelector('h1').textContent = evt.target.value
			}
		})
		layer.querySelector('button[name="deleteBtn"]').addEventListener('click', () => {
			layer.__layer__.svg.parentNode.removeChild(layer.__layer__.svg)
			layer.parentNode.removeChild(layer)
			document.forms.controllers.classList.add('hidden')
		})
		return layer
	},
	new: function() {
		const newLayer = layers.create()
		newLayer.__layer__ = {
			// initialized: [],
			svg: layers.addSvgLayer()
		}
		el.layerList.appendChild(newLayer)
		layers.select(newLayer)
	},
	addSvgLayer: function() {
		const layerSvg = svg(`
			<g class="layer">
				${ _for(metadata.lanes, () => `
					<g className="lane">
						<g className="visual"></g>
					</g>
				`)}
			</g>
		`)
		el.svg.appendChild(layerSvg)
		return layerSvg
	},
	current: () => document.querySelector('li.layer.current'),
	select: function(layer) {
		if (layers.current() === layer) return
		if (layers.current()) {
			layers.current().__layer__.svg.classList.remove('current')
			layers.current().classList.remove('current')
		}
		layer.__layer__.svg.classList.add('current')
		layer.classList.add('current')

		// show properties
		document.forms.controllers.classList.remove('hidden')

		// update layer name in properties
		document.forms.controllers.querySelector('h1').textContent = layer.querySelector('input[name="name"]').value

		// update properties
		const layerData = layer.__layer__.properties
		if (layerData) {
			if (!layerData.category) return
			controllers.category.value = layerData.category
			controllers.category.dispatchEvent(new Event('change', {bubbles: false}))

			if (!layerData.selectType.visType) return
			const inputVisType = document.querySelector(`input[type="radio"][name="visType"][value="${layerData.selectType.visType}"]`)
			inputVisType.checked = true
			inputVisType.dispatchEvent(new Event('change', {bubbles: false}))

			for (const fieldset in layerData) {
				if (['category', 'selectType'].includes(fieldset)) continue
				for (const field in layerData[fieldset]) {
					// if(!controllers[fieldset].elements[field]) debugger
					controllers[fieldset].elements[field].value = layerData[fieldset][field]
				}
			}
		} else {
			controllers.category.value = ''
			controllers.category.dispatchEvent(new Event('change'))
		}
	},
	update: function(frame) {
		for (const layer of document.querySelectorAll('li.layer')) {
			layers.updateLayer(layer.__layer__, frame)
		}
	},
	all: function() {
		return Array.from(document.querySelectorAll('li.layer'))
			.map(el => el.__layer__)
	},
	updateLayer: function(layer, frame) {
		const props = layer.properties
		if (!props) return
		if (!layer.properties.selectType) return
		if (!layer.properties.selectType.visType) return

		const currentVis = visuals.get(layer.properties.selectType.visType)
		const svgLayer = layer.svg
		const svgLaneWidth = SVG_WIDTH
		const svgLaneHeight = SVG_HEIGHT / metadata.lanes
		const x = layer.properties.transform.x
		const y = layer.properties.transform.y
		const moveWithSwimmer = layer.properties.transform.move === 'on'
		const shouldFlip = layer.properties.transform.flip === 'on'

		for (let lane = 0; lane < metadata.lanes; lane++) {
			if (!frame[lane]) continue

			const svgLane = svgLayer.children[lane]
			svgLane.removeAttribute('style')

			// display filter by lanes
			if (props.display.filter === 'lanes' && props.display[`lane-${lane}`] !== 'on') {
				svgLane.style.display = 'none'
				continue
			} else {
				svgLane.style.display = null
			}


			const flipFactor = !shouldFlip ? 1 : frame[lane].direction === 'advance' ? 1 : -1
			const svgX = (moveWithSwimmer ? 1 : 0) * (frame[lane].x_middle / halfDistance * svgLaneWidth) +
				x * flipFactor * svgLaneWidth / 100 + (!moveWithSwimmer && flipFactor === -1 ? svgLaneWidth : 0)
			const svgY = svgLaneHeight / 2 + y * svgLaneHeight / 100

			svgLane.style.clipPath = `url(#cp-lane)`
			svgLane.style.opacity = layer.properties.transform.opacity
			svgLane.style.transform = `translate(0px, ${lane * svgLaneHeight}px)`
			svgLane.firstElementChild.style.transform = `translate(${svgX}px, ${svgY}px) rotate(${layer.properties.transform.r}deg)`
			svgLane.style.fontFamily = layer.properties.text?.font
			svgLane.style.fontSize = layer.properties.text?.fontSize
			svgLane.style.fill = (layer.properties.fill && '#' + layer.properties.fill.hex + percentInHex(layer.properties.fill.transparency))
			svgLane.style.stroke = (layer.properties.stroke && '#' + layer.properties.stroke.hex + percentInHex(layer.properties.stroke.transparency))
			svgLane.style.strokeWidth = layer.properties.stroke?.width
			svgLane.style.textAnchor = 'middle'
			svgLane.style.dominantBaseline = 'middle'
			// if currentLayer
			//controllers.style.elements.generatedCss.value = svgLane.style.cssText.replaceAll('; ', ';\n')
			svgLane.setAttribute(
				'style',
				svgLane.style.cssText + '\n' + layer.properties.style.css
			)

			const visThis = {
				...currentVis,
				flipFactor: flipFactor
			}
			if (layer.properties.transform.w) {
				visThis.w = layer.properties.transform.w * svgLaneWidth
			}
			if (layer.properties.transform.h) {
				visThis.h = layer.properties.transform.h * svgLaneHeight
			}

			if (currentVis.type !== 'text')
			for (const propset in currentVis.schema) {
				if (ignoredProperties.includes(propset)) continue
				if (currentVis.schema[propset].type) {
					const prop = currentVis.schema[propset]
					if (prop.type === 'fill' || prop.type === 'stroke') {
						visThis[propset] = '#' + layer.properties[propset].hex + percentInHex(layer.properties[propset].transparency)
					} else {
						throw new Error(`undefined prop type ${prop.type}`)
					}
				}/* else {
					for (const propName in currentVis.schema[propset]) {
						const prop = currentVis.schema[propset][propName]
						debugger
					}
				}*/
			}

			if (!layer.initialized) continue

			if (!layer.initialized[lane] && currentVis.svg.init) {
				svgLane.firstElementChild.innerHTML = currentVis.svg.init.call(visThis, frame[lane])
				layer.initialized[lane] = true
			}
			if (currentVis.svg.update) {
				currentVis.svg.update.call(visThis, svgLane.firstElementChild.firstElementChild, frame[lane])
			}
		}
	},
	// export: function() {
	// 	console.log(JSON.stringify(layers.all()))
	// },
	// import: function(json) {
	//
	// }
}
// addLayer, deleteLayer, moveLayer, renameLayer, toggleDisplayLayer
document.forms.controllers.addEventListener('change', () => {
	const layer = layers.current()
	const data = getPropertyValues()
	layer.__layer__.properties = data
	console.log('save changes into ', layer.querySelector('input[name="name"]').value, layer, data)
})
flayers.addBtn.addEventListener('click', () => {
	layers.new()
})

document.forms.controls.current.addEventListener('pointerdown', () => {
	el.video.pause()
})
document.forms.controls.current.addEventListener('change', evt => {
	el.video.currentTime = evt.target.value
})
document.forms.controls.progressBar.addEventListener('change', evt => {
	el.video.currentTime = evt.target.value / 100 * el.video.duration
})