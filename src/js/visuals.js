const callbacks = {}
/*function register(id, callback) {
	if (callbacks[id]) {
		throw `visual ${id} already registered`
	} else {
		// console.log(`${id} registered`)
	}
	callbacks[id] = callback
}*/

function categories() {
	return Object.keys(callbacks)
}

function category(id) {
	return callbacks[id]
}

function registerCategory(category) {
	for (const categoryName in category) {
		const visuals = category[categoryName]
		callbacks[categoryName] = {}
		for (const visualName in visuals) {
			const visual = visuals[visualName]
			callbacks[categoryName][visualName] = visual
		}
	}
}

function register(module) {
	if (typeof module === 'string') return

	if (!module.category) throw new Error('Module need a category')
	if (!module.type) throw new Error('Module need a type')
	// if (!module.draw) throw new Error('Module need a draw function')
	if (!module.schema) module.schema = {}
	// TODO check svg

	if (!callbacks[module.category]) {
		callbacks[module.category] = {}
	}

	if (callbacks[module.category][module.type]) {
		throw new Error(`Module ${module.category}.${module.type} already defined`)
	}

	callbacks[module.category][module.type] = module
}

function get(id) {
	const [category, name] = id.split('.')
	return callbacks[category][name]
}
function load(list) {
	return Promise.all(list.map(name => import(`./visuals/${name}.js`).then(registerCategory)))
}

export const visuals = {
	register,
	load,
	get,
	category,
	categories
}